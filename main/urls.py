from django.urls import path

from .views import index, project, blog, detail_blog

urlpatterns = [
    path('', index, name='index'),
    path('project/', project, name='project'),
        path('blog/', blog, name='blog'),
    path('blog/<int:id>', detail_blog, name="detail_blog")
]